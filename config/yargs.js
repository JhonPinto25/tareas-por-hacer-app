const opts = {
    descripcion: {
        demand: true,
        alias: 'd',
        completado: true,
        desc: 'Descripcion de la tarea por hacer'
    },
    completado: {

        default: true,
        alias: 'c',
        desc: 'Descripcion de la tarea por hacer'
    }
}

const argv = require('yargs')
    .command('crear', 'crea un elemento por hacer', opts)
    .command('actualizar', 'Actualiza el esatdo completado de una tarea', opts)
    .command('mostrar', 'Muestra las tareas por hacer', opts)
    .command('eliminar', 'Elimina las tareas por hacer', opts)

.help()
    .argv;

module.exports = {
    argv
}